# Homework Week 3

## Using the following database diagram
![alt text](inventory.png)
* Replicate the DB Schema from the image in your DBMS using migrations
* Make sure to create the relations trough the tables
* Insert at least 20 products, 5 customers, 10 orders with their order details. to do this use seeders
  * for products
  * use stock between 1 and 25
  * Price should be between 10 and 100

## When the database is populated

### Make the following queries using active record

1. Select from what line an order belongs
2. Select All orders from an Order line
3. Select All orders that contains an X product
4. Select the total of sales of X product
5. Select All the customers who bought a product with price greater than $60, sorted by product name (include customer, product and order information)
6. Select All orders between dates X and Y
7. Count the total of customer who buy a product, with the amount of product ordered desc by total customer,
8. Select All the products a X Customer has bought ordered by date
9. Select the total amount of products a X customer bought between 2 dates
10. Select what is the most purchased product
11. Update products stock to 10 when stock is smaller than 3

### Make the following using models scope
12. Select All orders between dates X and Y for X Customer
13. Create filter name (default), and by price.
14. Select what is the most purchased product
15. Search product that contains words greater than two letters

### Add model validation use some custom errors message for stock and name for product
### Test validation

17. Create one product without name
18. Update a product without price
19. Create user without name

## EXECUTION

You can run the mehotds as:

First load development environment:
```shell
$ bin/rails c
```
Then run methods:
```shell
>> Order.belongs_to_order_line
```
