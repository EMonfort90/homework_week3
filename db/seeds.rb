# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

5.times do
  Customer.create(
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name,
    address: Faker::Address.street_address,
    phone: Faker::PhoneNumber.phone_number
  )
end

20.times do
  Product.create(
    sku: Faker::Number.number(digits: 5),
    name: Faker::Commerce.product_name,
    description: Faker::Lorem.paragraph(sentence_count:3),
    price: Faker::Commerce.price(range: 10..100.0),
    stock: Faker::Number.between(from: 1, to: 25)
  )
end

10.times do
  Order.create(
    date: Faker::Date.between(from: '2021-09-01', to: '2021-11-30'),
    customer_id: Faker::Number.between(from: 1, to: 5),
    total: Faker::Commerce.price(range: 10.0, to: 250.0),
    status: Faker::Lorem.sentence(word_count: 1)
  )
end

25.times do
  OrderLine.create(
    order_id: Faker::Number.between(from: 1, to: 10),
    product_id: Faker::Number.between(from: 1, to: 20),
    quantity: Faker::Number.between(from: 1, to: 5),
    price: Faker::Commerce.price(range: 10..100.0),
    total: Faker::Commerce.price(range: 10..500.0)
  )
end
