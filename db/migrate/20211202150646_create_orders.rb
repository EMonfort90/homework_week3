# frozen_string_literal: true

# Migration that creates orders table
class CreateOrders < ActiveRecord::Migration[6.1]
  def change
    create_table :orders do |t|
      t.date :date
      t.float :total
      t.string :status

      t.timestamps
    end
  end
end
