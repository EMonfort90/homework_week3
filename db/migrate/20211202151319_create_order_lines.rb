# frozen_string_literal: true

# Migration that creates order_lines table
class CreateOrderLines < ActiveRecord::Migration[6.1]
  def change
    create_table :order_lines do |t|
      t.integer :quantity
      t.float :price
      t.float :total

      t.timestamps
    end
  end
end
