# frozen_string_literal: true

# Migration that adds foreign key in order_lines pointing to products
class AddProductRefToOrderLine < ActiveRecord::Migration[6.1]
  def change
    add_reference :order_lines, :product, null: false, foreign_key: true
  end
end
