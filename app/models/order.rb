# frozen_string_literal: true

# Class that represents orders
class Order < ApplicationRecord
  belongs_to :customer
  has_many :order_line

  # 12 Select All orders between dates X and Y for X Customer
  scope :all_orders_between_dates, -> { where('customer_id = 1 AND date > "2021-09-10" AND date < "2021-11-10"') }

  # 1 Select from what line an order belongs to
  def self.belongs_to_order_line
    orders = joins(:order_line).where('order_lines.order_id = 3 and order_lines.product_id = 9')
    orders.each { |order| puts "Order id: #{order.id}" }
  end

  # 3 Select all orders that contain X product
  def self.orders_that_contain_product
    orders = Order.joins(:order_line).where('order_lines.product_id = 9')
    orders.each { |order| puts "Order id: #{order.id}" }
  end

  # 6 Select All orders between dates
  def self.between_dates
    orders = where('date > "2021-09-10" AND date < "2021-11-10"')
    orders.each { |order| puts "Order: #{order.id} Date: #{order.date}" }
  end

  # 9 Select the total amount of products a X customer bought between 2 dates
  def self.total_amount_bought
    sales = select('sum(order_lines.quantity) as total_products')
            .joins(:order_line)
            .where('orders.customer_id = 4 AND date > "2021-09-15" AND date < "2021-10-15"')
    puts sales[0].total_products
  end
end
