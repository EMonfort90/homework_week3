# frozen_string_literal: true

# Class that represents a customers, and queries of it
class Customer < ApplicationRecord
  has_many :order

  validates :first_name, presence: { message: 'A customer frist name must be provided' }
  validates :last_name, presence: { message: 'A customer last name must be provided' }

  # 5 Select All the customers who bought a product with price greater than $60, sorted by product name
  # (include customer, product and order information)
  def self.bought_more_expensive_than_sixty
    sales = select('customers.first_name, customers.last_name,
                    orders.id AS order_id, orders.date,
                    products.id as product_id, products.name, products.price')
            .joins(order: { order_line: :product }).where('products.price > 60').order('products.price DESC')
    sales.each do |sale|
      puts "Customer: #{sale.first_name} #{sale.last_name}" \
           " Product: #{sale.product_id} #{sale.name} - $#{sale.price}" \
           " Order: #{sale.order_id} at #{sale.date}"
    end
  end

  # 7 Count the total of customer who buy a product, with the amount of product ordered desc by total customer
  def self.total_amount_bought
    sales = select('count(DISTINCT customers.id) as total_customers, sum(order_lines.quantity) as total_products,
                    order_lines.product_id')
            .joins(order: :order_line)
            .group('order_lines.product_id')
            .order('total_customers DESC')
    sales.each do |sale|
      puts "Total Custmores: #{sale.total_customers}" \
           " Product id: #{sale.product_id} Amount of product: #{sale.total_products}"
    end
  end

  # 18 Create user without name
  def self.create_without_name
    create!(address: '4004 Oliver Street', phone: '972-333-6210')
  rescue ActiveRecord::RecordInvalid => e
    puts e
  end
end
