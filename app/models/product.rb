# frozen_string_literal: true

# Class that represents products
class Product < ApplicationRecord
  include ActiveModel::Validations
  attr_accessor :name, :price

  validates :name, presence: { message: 'A product name must be provided.' }
  validates :price, presence: { message: 'Can\'t update a product without a price.' }
  has_many :order_line

  # 13.1 Filter by name
  scope :name_filter, ->(input = 'duty') { where('name LIKE ?', "%#{input}%") }

  # 13.2 Filter by price
  scope :price_filter, ->(lower = 30, higher = 60) { where('price >= ? AND price <= ?', lower, higher) }

  # 14 Most purchased product
  scope :most_purchased, lambda {
    select('products.*, sum(order_lines.quantity) AS total_sales')
      .joins(:order_line)
      .group('products.id')
      .order('total_sales DESC')
      .limit(1)
  }

  # 15 Search product that contains words greater than two letters
  scope :greater_than_two_letters, -> { where('name LIKE "%___%"') }

  # 8 Select All the products a X Customer has bought ordered by date
  def self.bought_by_customer
    products = select('products.name, orders.date')
               .joins(order_line: :order)
               .where('orders.customer_id = 1')
               .order('orders.date DESC')
    products.each { |product| puts "Product: #{product[:name]} - Date: #{product.date}" }
  end

  # 10 Select what is the most purchased product
  def self.most_purchased_product
    product = select('products.id, products.name, SUM(order_lines.quantity) AS total_sales')
              .joins(:order_line)
              .group('products.id')
              .order('total_sales DESC')
              .take
    puts "product: #{product.id} - #{product[:name]}. Total sales: #{product.total_sales}"
  end

  # 11 Update products stock to 10 when stock is smaller than 3
  def self.restock
    update_all('stock = 10 where stock < 3')
  end

  # 16 Create one product without name
  def self.create_product_without_name
    Product.create!(sku: 16_874, description: 'this is a product', price: 28.47, stock: 7)
  rescue ActiveRecord::RecordInvalid => e
    puts e
  end

  # 17 Update a product without price
  def self.update_without_price
    product = Product.find(5)
    product.update!(price: nil)
  rescue ActiveRecord::RecordInvalid => e
    puts e
  end
end
