# frozen_string_literal: true

# Class that represents Order Lines
class OrderLine < ApplicationRecord
  belongs_to :order
  belongs_to :product

  # 2_Select all order lines from an order
  def self.order_lines_from_order
    order_lines = where(order_id: 3)
    order_lines.each { |ol| puts "prduct id: #{ol.product_id} - order id: #{ol.order_id}" }
  end

  # 4_Select the total sales of X product
  def self.total_sales_of_product
    ol = select('product_id, sum(quantity) as total_sales')
         .where('product_id = 1')
    puts "Product id: #{ol[0].product_id} - Total Sales: #{ol[0].total_sales}"
  end
end
